#include <iostream>
#include <math.h>
// 1. Создайте класс вектора.
//    Его переменные должны быть в приватной области видимости.
//    Показывать значения этих переменных нужно через публичный метод.
class Vector
{
private:
    double x;
    double y;
    double z;
public:
    Vector() : x(0), y(0), z(0)
    {}
    Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
    {}
    void Show()
    {
        std::cout << x << " " << y << " " << z << "\n";
    }
 // 2. Дополните класс Vector public методом, который будет возвращать длину (модуль) вектора. 
    double VectorLength()
    {
        return sqrt(x * x + y * y + z * z);
    }
};

int main()
{
// 3. Протестируйте проект.
    Vector t1(3, 3, 3);
    Vector t2(-3, 3, 3);
    Vector t3(-4, 4, -4);
    Vector t4(-4, -4, -4);
    t1.Show();
    std::cout << t1.VectorLength() << "\n";
    t2.Show();
    std::cout << t2.VectorLength() << "\n";
    t3.Show();
    std::cout << t3.VectorLength() << "\n";
    t4.Show();
    std::cout << t4.VectorLength() << "\n";
}